package tv.zlatko.jdemo.school;


import javax.swing.event.ListDataEvent;

public interface SchoolListener {
    void klassesChanged(School s);
    void teachersChanged(ListDataEvent s);
}
