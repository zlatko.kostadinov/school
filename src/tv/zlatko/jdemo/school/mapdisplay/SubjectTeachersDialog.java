package tv.zlatko.jdemo.school.mapdisplay;

import tv.zlatko.jdemo.school.School;
import tv.zlatko.jdemo.school.SchoolListener;
import tv.zlatko.jdemo.school.klas.Klas;
import tv.zlatko.jdemo.school.klas.SchoolController;
import tv.zlatko.jdemo.school.subject.*;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Vector;

public class SubjectTeachersDialog extends JDialog {

    private final JList<Teacher> teachers;
    private final JList<Klas> klasses = new JList<>();
    private final School school;
    private final JComboBox<Subject> subjects;

    public SubjectTeachersDialog(Frame owner, String title, SchoolController schoolController) {
        super(owner, title);
        school = schoolController.getSchool();
        setLayout(new BorderLayout());
        final JPanel north = new JPanel(new BorderLayout());
        add(north, BorderLayout.NORTH);
        final java.util.List<Subject> ss = school.getTeachersMap().getSubjects();
        subjects = new JComboBox<>(new DefaultComboBoxModel<>(ss.toArray(new Subject[ss.size()])));
        subjects.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Subject s = (Subject) value;
                return super.getListCellRendererComponent(list, s.getDisplayValue(), index, isSelected, cellHasFocus);
            }
        });
        north.add(subjects);

        final JPanel center = new JPanel(new FlowLayout());
        add(center, BorderLayout.CENTER);
        teachers = new JList<>(new TeacherListModel(school));
        teachers.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Teacher t = (Teacher) value;
                return super.getListCellRendererComponent(list,
                        String.format("[%s], %s", t.getId(), t.getName()),
                        index, isSelected, cellHasFocus);
            }
        });
        final JPanel teachersPanel = new JPanel(new BorderLayout());
        teachersPanel.setBorder(new TitledBorder(new EtchedBorder(), "Учители"));
        teachersPanel.add(new JScrollPane(teachers));

        final JPanel klassesPanel = new JPanel(new BorderLayout());
        klassesPanel.setBorder(new TitledBorder(new EtchedBorder(), "Класове"));
        klassesPanel.add(new JScrollPane(klasses), BorderLayout.CENTER);
        final JPanel klassesRight = new JPanel(new BorderLayout());
        klassesPanel.add(klassesRight, BorderLayout.EAST);
        final JPanel klassesButtons = new JPanel(new GridLayout(2, 1, 0, 5));
        klassesRight.add(klassesButtons, BorderLayout.NORTH);
        final AbstractAction addKlasForTeacherAction = new AbstractAction(" + ") {
            @Override
            public void actionPerformed(ActionEvent e) {
                final Object select = JOptionPane.showInputDialog(klasses, "Избери клас на който учителя преподава", "Избери клас",
                        JOptionPane.PLAIN_MESSAGE, null, school.getKlasses().toArray(), null);
                if (select == null) {
                    return;
                }

                school.getTeachersMap().addTeacher(new Key((Klas) select, (Subject) subjects.getSelectedItem()),
                        teachers.getSelectedValue());
                saveTeachers(teachers, schoolController);
            }
        };
        klassesButtons.add(new JButton(addKlasForTeacherAction));
        final AbstractAction removeKlasForTeacherAction = new AbstractAction(" - ") {
            @Override
            public void actionPerformed(ActionEvent e) {
                school.getTeachersMap().removeTeacher(new Key(klasses.getSelectedValue(),
                        subjects.getItemAt(subjects.getSelectedIndex())));
                saveTeachers(teachers, schoolController);
            }
        };
        removeKlasForTeacherAction.setEnabled(false);
        klassesButtons.add(new JButton(removeKlasForTeacherAction));

        final JPanel panel2 = new JPanel(new GridLayout(1, 2));
        panel2.add(teachersPanel);
        panel2.add(klassesPanel);
        center.add(panel2);
        final JPanel teacherButtons = new JPanel(new GridLayout(3, 1, 0, 5));
        final JPanel buttonsUp = new JPanel(new BorderLayout());
        buttonsUp.add(teacherButtons, BorderLayout.NORTH);
        teachersPanel.add(buttonsUp, BorderLayout.EAST);

        teacherButtons.add(new JButton(new AbstractAction("+") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final String newName = readTeacherName(teachers, false);
                if (newName == null) return;

                school.addTeacher(newName);
                saveTeachers(teachers, schoolController);
            }
        }));
        teacherButtons.add(new JButton(new AbstractAction("-") {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (teachers.getSelectedIndex() < 0) {
                    JOptionPane.showMessageDialog(teachers, "Please select teacher to remove!");
                    return;
                }

                school.removeTeacher(teachers.getSelectedIndex());
                saveTeachers(teachers, schoolController);
            }
        }));
        teacherButtons.add(new JButton(new AbstractAction("Rename") {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (teachers.getSelectedIndex() < 0) {
                    JOptionPane.showMessageDialog(teachers, "Please select teacher to rename!");
                    return;
                }

                final String newName = readTeacherName(teachers, true);
                if (newName == null) return;

                school.renameTeacher(teachers.getSelectedIndex(), newName);
                saveTeachers(teachers, schoolController);
            }
        }));
        klasses.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                removeKlasForTeacherAction.setEnabled(klasses.getSelectedIndex() >= 0);

            }
        });
        teachers.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                addKlasForTeacherAction.setEnabled(teachers.getSelectedIndex() >= 0);
                loadKlassesForSelectedTeacher();
            }
        });
        subjects.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    loadKlassesForSelectedTeacher();
                }
            }
        });
        school.getTeachersMap().addListener(new TeachersMap.Listener() {
            @Override
            public void mapChanged(TeachersMap source) {
                loadKlassesForSelectedTeacher();
            }
        });

        addKlasForTeacherAction.setEnabled(teachers.getSelectedIndex() >= 0);
        loadKlassesForSelectedTeacher();

        setLocation(owner.getX() + owner.getWidth(), owner.getY());
        pack();
    }

    private void loadKlassesForSelectedTeacher() {
        final DefaultListModel<Klas> model1 = new DefaultListModel<>();
        final Klas[] klasses1 = school.getTeachersMap().getKlasses(teachers.getSelectedValue(),
                (Subject) subjects.getSelectedItem());
        model1.clear();
        for (Klas klas : klasses1) {
            model1.addElement(klas);
        }
        klasses.setModel(model1);
    }

    private  void saveTeachers(JList teachers, SchoolController schoolController){
        try{
            schoolController.save();
        }catch(IOException ex){
            JOptionPane.showMessageDialog(teachers, "Error saving to file");
        }
    }

    private String readTeacherName(JList<Teacher> teachers, boolean isRename) {

        String newName = null;

        boolean userInputInvalid = false;

        while (!userInputInvalid) {

            final String selectedName;
            if (isRename && teachers.getSelectedIndex() >= 0) {
                selectedName = teachers.getSelectedValue().getName();
            } else {
                selectedName = null;
            }
            newName = JOptionPane.showInputDialog(teachers, "Enter teacher name!", selectedName);
            if (newName == null) {
                return null;
            }

            if (newName.length() == 0) {
                JOptionPane.showMessageDialog(teachers, "No empty name allowed!");
                return null;
            }

            final String regex = "[A-Z][a-z]* [A-Z][a-z]*";
            if (!newName.matches(regex)) {
                JOptionPane.showMessageDialog(teachers, "Please input two names with Capital case");
            } else {
                userInputInvalid = true;
            }

        }

        return newName;
    }
}
