package tv.zlatko.jdemo.school.DataStorage;

/**
 * Created by zlatko on 09.02.18.
 */
public class StudentReaderException extends Exception {

    public StudentReaderException(final String message) {
        super(message);
    }

    public StudentReaderException() {
        super("Error reading students file!");
    }
}
