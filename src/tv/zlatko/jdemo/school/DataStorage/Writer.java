package tv.zlatko.jdemo.school.DataStorage;

import tv.zlatko.jdemo.school.School;
import tv.zlatko.jdemo.school.Student;
import tv.zlatko.jdemo.school.klas.Klas;
import tv.zlatko.jdemo.school.subject.Key;
import tv.zlatko.jdemo.school.subject.Teacher;
import tv.zlatko.jdemo.school.subject.TeachersMap;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Victor on 01.02.18.
 */

public class Writer {

    private BufferedWriter target;

    public Writer(String file) throws IOException {
        this.target = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
    }


    public void writeData(Klas klas) throws IOException {
        target.write(klas.getName());
        target.newLine();
        for (Student s : klas.getStudents()) {
            target.write(s.getName() + "#" + s.getDob());
            target.newLine();
        }
        target.write("***");
        target.newLine();
    }

    public void writeTeachers(School s) throws IOException{
        target.write("###");
        target.newLine();
        for (Teacher t : s.getTeachers()) {
            target.write(String.format("%s -> %s", t.getId(), t.getName()));
                writeSubjects(t, s.getTeachersMap());
            target.newLine();
        }
    }

    private void writeSubjects(Teacher t, TeachersMap m)  throws IOException{
        target.newLine();
        for (Map.Entry<Key, Teacher> entry : m.getMap().entrySet()) {
            if(entry.getValue().getName().equals(t.getName())){
               target.write(entry.getKey().getSubject()+", "+entry.getKey().getKlas().toString()+'|');
            }
        }
    }

    public void close() throws IOException {
        target.close();
    }
}

