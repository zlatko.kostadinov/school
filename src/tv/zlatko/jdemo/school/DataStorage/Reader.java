package tv.zlatko.jdemo.school.DataStorage;

import tv.zlatko.jdemo.school.Student;
import tv.zlatko.jdemo.school.klas.Klas;
import tv.zlatko.jdemo.school.subject.Key;
import tv.zlatko.jdemo.school.subject.Subject;
import tv.zlatko.jdemo.school.subject.Teacher;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Victor on 01.02.18.
 */

public class Reader {
    private List<Klas> klasses = new LinkedList<>();
    private List<Teacher> teachers = new LinkedList<>();
    private final Map<Key, Teacher> map = new HashMap<>();

    public Reader(File file) throws StudentReaderException {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file, StandardCharsets.UTF_8.name());
            while (scanner.hasNextLine()) {
                final String name = scanner.nextLine();
                if(name.equals("###")) break;
                    readOneKlas(scanner, name);
            }
            readTeachers(scanner);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new StudentReaderException();
        } finally {
            if (scanner != null)
                scanner.close();
        }
    }

    private void readOneKlas(Scanner scanner, String name) throws StudentReaderException {
        if(name.equals("***")) return;
        final Klas klas = new Klas(name);
        klasses.add(klas);
        while (scanner.hasNextLine()) {
            String string = scanner.nextLine();
            if ("***".equals(string)) {
                break;
            }
            String[] data = string.split("#");
            DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

                String dateTxt = data[1];
                try {
                    Date date = format.parse(dateTxt);
                    klas.addStudent(new Student(data[0], date));
                } catch (ParseException ex) {
                    throw new StudentReaderException("Error parsing date!");
                }

        }
    }

    private void readTeachers(Scanner scanner){
        while (scanner.hasNextLine()){
            String[] teacher = scanner.nextLine().split(" -> ");
            Teacher t = new Teacher(Integer.parseInt(teacher[0]), teacher[1]);
            teachers.add(t);
            readKeys(scanner, t);
        }
    }

    private void readKeys(Scanner scanner, Teacher t){
        String[] keys = scanner.nextLine().split("[|]");
        if(keys[0].equals("")) return;
        Arrays.stream(keys).filter(x -> !x.equals(""));
        for (String key : keys) {
            String[] k = key.split(", ");
            Subject v = null;
            for (Subject s : Subject.values()) {
                if(s.getDisplayValue().equals(k[0]))
                    v=s;
            }
            map.put(new Key(new Klas(k[1]), v), t);
        }
    }

    public Collection<Klas> getClasses() {return Collections.unmodifiableCollection(klasses);}
    public Collection<Teacher> getTeachers() {return Collections.unmodifiableCollection(teachers);}
    public Map<Key, Teacher> getMap() {return map;}
}

