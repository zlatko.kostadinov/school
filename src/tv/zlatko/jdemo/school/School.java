package tv.zlatko.jdemo.school;

import tv.zlatko.jdemo.school.klas.Klas;
import tv.zlatko.jdemo.school.subject.Key;
import tv.zlatko.jdemo.school.subject.Teacher;
import tv.zlatko.jdemo.school.subject.TeachersMap;

import javax.swing.event.ListDataEvent;
import java.util.*;

public class School {

    private int nextId = 0;

    private List<SchoolListener> listeners = new LinkedList<>();

    private List<Klas> klasses = new ArrayList<>();
    private List<Teacher> teachers = new ArrayList<>();

    public List<Klas> getKlasses() {
        return Collections.unmodifiableList(klasses);
    }

    public List<Teacher> getTeachers() {
        return Collections.unmodifiableList(teachers);
    }

    public void addListener(SchoolListener l) {
        listeners.add(l);
    }

    public void removeListener(SchoolListener l) {
        listeners.remove(l);
    }

    private final TeachersMap teachersMap = new TeachersMap();

    public void addTeacherToMap(Key k, Teacher t){
        teachersMap.addTeacher(k,t);
    }

    public void addKlas(String name) {
        klasses.add(new Klas(name));
        fireKlassesChanged();
    }

    public void addTeacher(String name) {
        teachers.add(new Teacher(nextId, name));
        nextId ++;

        final ListDataEvent dataEvent = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED,
                teachers.size() - 1, teachers.size() - 1);
        fireTeachersChanged(dataEvent);
    }

    public void addTeacher(int id, String name) {
        teachers.add(new Teacher(id, name));
        nextId=++id;

        final ListDataEvent dataEvent = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED,
                teachers.size() - 1, teachers.size() - 1);
        fireTeachersChanged(dataEvent);
    }

    public void removeTeacher(int index) {
        teachers.remove(index);

        final ListDataEvent dataEvent = new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, index, index);
        fireTeachersChanged(dataEvent);
    }

    public void renameTeacher(int index, String newName) {
        teachers.get(index).setName(newName);

        final ListDataEvent dataEvent = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, index, index);
        fireTeachersChanged(dataEvent);
    }

    private void fireTeachersChanged(ListDataEvent dataEvent) {
        for (SchoolListener sl : listeners) {
            sl.teachersChanged(dataEvent);
        }
    }

    private void fireKlassesChanged() {
        for (SchoolListener sl : listeners) {
            sl.klassesChanged(this);
        }
    }

    public void deleteTeacher(int teacherIndex) {
        teachers.remove(teacherIndex);
        fireTeachersChanged(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, teacherIndex, teacherIndex));
    }

    public void addKlas(Klas k) {
        klasses.add(k);
        fireKlassesChanged();
    }

    public void deleteKlas(int klasIndex) {
        klasses.remove(klasIndex);
        fireKlassesChanged();
    }

    public TeachersMap getTeachersMap() {
        return teachersMap;
    }
}

