package tv.zlatko.jdemo.school.subject;

/**
 * Created by zlatko on 16.03.18.
 */
public enum Subject {
    LITERATURE("Литература"),
    RODEN_KRAJ("Роден край"),
    MUSIC("Музика"),
    GEORGAPHY("География"),
    HISTORY("История"),
    PHISYCS("Физика"),
    CHEMISTRY("Химия"),
    ENGLISH("Английски език"),
    MATH("Математика"),
    INFORMATICS("Информатика");


    private final String displayValue;

    Subject(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }


    @Override
    public String toString() {
        return getDisplayValue();
    }
}
