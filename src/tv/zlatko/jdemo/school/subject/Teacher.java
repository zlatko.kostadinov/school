package tv.zlatko.jdemo.school.subject;

import java.util.*;

/**
 * Created by zlatko on 16.03.18.
 */
public class Teacher {
    private String name;
    private final int id;

    public Teacher(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
}
