package tv.zlatko.jdemo.school.subject;

import tv.zlatko.jdemo.school.klas.Klas;
import java.util.Objects;
/**
 * Created by Victor on 14.04.18.
 */
public class Key {
    private final Klas klas;
    private final Subject subject;

    public Key(Klas klas, Subject subject) {
        this.subject = subject;
        this.klas = klas;
    }

    public Klas getKlas(){
        return klas;
    }

    public Subject getSubject(){
        return subject;
    }

    @Override
    public int hashCode() {
        // return klas.getName().hashCode() + subject.hashCode();
        return Objects.hash(klas.getName(), subject.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Key))
            return false;
        return this.subject.equals(((Key)obj).subject) && this.klas.equals(((Key)obj).klas);
    }
}
