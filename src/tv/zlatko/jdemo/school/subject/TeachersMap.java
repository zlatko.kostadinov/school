package tv.zlatko.jdemo.school.subject;

import tv.zlatko.jdemo.school.klas.Klas;

import javax.swing.event.EventListenerList;
import java.util.*;

/**
 * Created by zlatko on 16.03.18.
 */
public class TeachersMap {

    private final Map<Key, Teacher> map = new HashMap<>();

    public List<Teacher> getTeachers() {return new ArrayList<>(new HashSet<>(map.values()));}

    public Map<Key, Teacher> getMap() {
        return Collections.unmodifiableMap(map);
    }

    public List<Subject> getSubjects() {return Arrays.asList(Subject.values());}
    public List<Teacher> getTeachers(Subject subject) {
        final HashSet<Teacher> value = new HashSet<>();
        for (Key key : map.keySet()) {
            if (key.getSubject() == subject) {
                value.add(map.get(key));
            }
        }
        return new ArrayList<>(value);
    }
    public List<Subject> getSubjects(Teacher teacher) {
        final HashSet<Subject> values = new HashSet<>();
        for (Map.Entry<Key, Teacher> entry : map.entrySet()) {
            if (entry.getValue().equals(teacher)) {
                values.add(entry.getKey().getSubject());
            }
        }
        return new ArrayList<>(values);
    }

    public Teacher getTeacher(Klas k, Subject s) {
        return map.get(new Key(k, s));
    }

    public void addTeacher(Key k, Teacher t) {
        map.put(k, t);
        fireMapChanged();
    }

    public void removeTeacher(Key k){
        map.remove(k);
        fireMapChanged();
    }

    public Klas[] getKlasses(Teacher teacher, Subject selectedItem) {
        final HashSet<Klas> values = new HashSet<>();

        for (Map.Entry<Key, Teacher> entry : map.entrySet()) {
            if(teacher==null) break;
            if (entry.getValue().getName().equals(teacher.getName()) && entry.getKey().getSubject().equals(selectedItem)) {
                values.add(entry.getKey().getKlas());
            }
        }

        return (Klas[])new ArrayList<>(values).toArray(new Klas[values.size()]);
    }

    private final LinkedList<Listener> listeners = new LinkedList<>();

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public void fireMapChanged() {
        for (Listener listener : listeners) {
            listener.mapChanged(this);
        }
    }

    public interface Listener {
        void mapChanged(TeachersMap source);
    }
}
