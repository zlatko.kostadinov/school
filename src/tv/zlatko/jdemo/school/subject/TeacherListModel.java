package tv.zlatko.jdemo.school.subject;



import tv.zlatko.jdemo.school.School;
import tv.zlatko.jdemo.school.SchoolListener;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import java.util.*;
/**
 * Created by Victor on 14.04.18.
 */
public class TeacherListModel extends DefaultListModel implements SchoolListener {
    private final List<Teacher> teachers = new LinkedList<>();

    public TeacherListModel(School source){
        source.addListener(this);
        teachers.addAll(source.getTeachers());
    }

    @Override
    public int getSize() {
        return teachers.size();
    }

    @Override
    public Object getElementAt(int index) {
        return teachers.get(index);
    }

    @Override
    public void klassesChanged(School s) {
        // do nothing
    }

    @Override
    public void teachersChanged(ListDataEvent dataEvent) {
        switch (dataEvent.getType()) {
            case ListDataEvent.INTERVAL_ADDED:
                for (int i = dataEvent.getIndex0(); i <= dataEvent.getIndex1(); i++) {
                    teachers.add(i, ((School) dataEvent.getSource()).getTeachers().get(i));
                }
                fireIntervalAdded(this, dataEvent.getIndex0(), dataEvent.getIndex1());
                break;
            case ListDataEvent.INTERVAL_REMOVED:
                for (int i = dataEvent.getIndex0(); i <= dataEvent.getIndex1(); i++) {
                    teachers.remove(dataEvent.getIndex0());
                }
                fireIntervalRemoved(this, dataEvent.getIndex0(), dataEvent.getIndex1());
                break;
            case ListDataEvent.CONTENTS_CHANGED:
                for (int i = dataEvent.getIndex0(); i <= dataEvent.getIndex1(); i++) {
                    teachers.set(i, ((School) dataEvent.getSource()).getTeachers().get(i));
                }
                fireContentsChanged(this, dataEvent.getIndex0(), dataEvent.getIndex1());
                break;
            default:
                break;
        }
    }
}
