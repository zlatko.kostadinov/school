package tv.zlatko.jdemo.school.klas;

import tv.zlatko.jdemo.school.DataStorage.Reader;
import tv.zlatko.jdemo.school.DataStorage.StudentReaderException;
import tv.zlatko.jdemo.school.DataStorage.Writer;
import tv.zlatko.jdemo.school.School;
import tv.zlatko.jdemo.school.subject.Key;
import tv.zlatko.jdemo.school.subject.Teacher;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Created by zlatko on 09.02.18.
 */
public class SchoolController {
    private final School school;

    public SchoolController(final School school) {
        this.school = school;
    }

    public void read() throws StudentReaderException {
        final File file = new File("data.txt");
        if (file.exists()) {
            Reader reader = new Reader(file);
            final Collection<Klas> klasses = reader.getClasses();
            final Collection<Teacher> teachers = reader.getTeachers();
            final Map<Key, Teacher> teacherMap = reader.getMap();

            for (Map.Entry<Key, Teacher> entry : teacherMap.entrySet()) {
                school.addTeacherToMap(entry.getKey(), entry.getValue());
            }

            for (Klas klass : klasses) {
                school.addKlas(klass);
            }

            for (Teacher teacher : teachers) {
                school.addTeacher(teacher.getId(), teacher.getName());
            }
        }
    }

    public void save() throws IOException {
        Writer writer = new Writer("data.txt");
        for (Klas klas : school.getKlasses()) {
            writer.writeData(klas);
        }
        writer.writeTeachers(school);
        writer.close();
    }

    public School getSchool(){
        return school;
    }
}
