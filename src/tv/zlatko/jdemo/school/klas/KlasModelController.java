package tv.zlatko.jdemo.school.klas;

import tv.zlatko.jdemo.school.Student;

import java.util.Date;

public class KlasModelController {
    private KlasTableModel model;
    public KlasModelController(final KlasTableModel model) {
        this.model = model;
    }

    public void addNewStudent(String name) {
        model.klas.addStudent(new Student(name, new Date()));
    }

    public void removeStudent(final int selectedStudentRow) {
        model.klas.deleteStudent(model.getStudentAtRow(selectedStudentRow));
    }

    public void renameStudent(String name, final int selectedStudentRow) {
        model.klas.renameStudent(name, model.getStudentAtRow(selectedStudentRow));
    }
}
