package tv.zlatko.jdemo.Main;

import java.util.Scanner;
public class Drawing {

    public static void main(String[] args) {

        Scanner UserInput = new Scanner(System.in);
        System.out.print("Enter number of levels: ");
        int EndNum = UserInput.nextInt();
        int count = 1;

        for(int l = 1; l<EndNum; l++) {
            for (int i = 0; i < count; i+=2) {
                for (int j = 0; j < EndNum - i; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k <= i; k++) {
                    System.out.print("* ");
                }
                System.out.println();
//                System.out.println("i = " + i);
//                System.out.println("count = " + count);
                count += 1;
            }
//            System.out.println();
        }
        UserInput.close();
    }
}
