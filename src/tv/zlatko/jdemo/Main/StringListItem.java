package tv.zlatko.jdemo.Main;

public class StringListItem {

    public String data;
    public StringListItem next;

    StringListItem(String data) {
        this.data = data;
    }

    public static void printAll(StringListItem start) {
        if (start == null) {
            System.out.println("<END>");
            return;
        }
        System.out.println(start.data);
        printAll(start.next);
    }

    public static void main(String[] args) {
        final StringListItem victor = new StringListItem("Victor");
        addElement(new StringListItem("Katq"), victor);
        addElement(new StringListItem("Toshko"), victor);
        addElement(new StringListItem("Georgi"), victor);
        printAll(victor);
        delete(victor, 1);
        printAll(victor);
    }

    public static StringListItem getLast(StringListItem start) {
        StringListItem next = start;
        while (next.next != null) {
            next = next.next;
        }
        return next;
    }

    public static void addElement(StringListItem newElement, StringListItem start) {
        getLast(start).next = newElement;
    }

    public static void delete(StringListItem start, int index) {
        StringListItem parent = start;
        for (int current = 0; current < index; current ++) {
            parent = parent.next;
        }
        parent.next = parent.next.next;
    }
}
